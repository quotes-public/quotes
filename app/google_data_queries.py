from app.clients.google_client import (
    GoogleClient,
    GoogleSignInResponse,
    GoogleSignUpResponse,
)

google_client = GoogleClient("https://identitytoolkit.googleapis.com/v1/accounts:")


def sign_up_user(email: str, password: str) -> GoogleSignUpResponse | None:
    result = google_client.sign_up(email, password)
    if result.is_err():
        return None
    return result.unwrap()


def sign_in_user(email: str, password: str) -> GoogleSignInResponse | None:
    result = google_client.sign_in(email, password)
    if result.is_err():
        return None
    return result.unwrap()
