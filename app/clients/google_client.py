from dataclasses import dataclass
from typing import Any

import requests
from dataclasses_jsonschema import JsonSchemaMixin
from result import Err, Ok, Result

from app.config import Config

JsonDict = dict[str, Any]


@dataclass
class BaseType(JsonSchemaMixin):
    pass


@dataclass(slots=True)
class GoogleSignInResponse(BaseType):
    localId: str
    email: str
    displayName: str
    idToken: str
    refreshToken: str
    expiresIn: str
    mfaInfo: list | None = None
    registered: bool | None = None
    providerUserInfo: list | None = None
    userNotifications: list | None = None
    mfaEnrollments: list | None = None
    mfaPendingCredential: str | None = None
    mfaEnrollmentId: str | None = None
    profilePicture: str | None = None


@dataclass(slots=True)
class GoogleSignUpResponse(BaseType):
    localId: str
    email: str
    idToken: str
    refreshToken: str
    expiresIn: str
    displayName: str | None = None
    mfaInfo: list | None = None
    registered: bool | None = None
    providerUserInfo: list | None = None
    userNotifications: list | None = None
    mfaEnrollments: list | None = None
    mfaPendingCredential: str | None = None
    mfaEnrollmentId: str | None = None
    profilePicture: str | None = None


@dataclass
class APIResult(BaseType):
    status: int


@dataclass
class APIError(APIResult):
    error: str


class GoogleClient:
    def __init__(self, base_url: str) -> None:
        self.base_url = base_url.rstrip("/")
        self.app_key = Config.GOOGLE_API_KEY

    def sign_in(self, email: str, password: str) -> Result[GoogleSignInResponse, APIError]:
        url = f"{self.base_url}signInWithPassword?key={self.app_key}"
        payload = {"email": email, "password": password, "returnSecureToken": True}
        result = requests.post(url, json=payload)
        if result.status_code != 200:
            return Err(APIError.from_dict(result.json()))
        return Ok(GoogleSignInResponse.from_dict(result.json()))

    def sign_up(self, email: str, password: str) -> Result[GoogleSignUpResponse, APIError]:
        url = f"{self.base_url}signUp?key={self.app_key}"
        payload = {"email": email, "password": password, "returnSecureToken": True}
        result = requests.post(url, json=payload)
        if result.status_code != 200:
            return Err(APIError.from_dict(result.json()))
        return Ok(GoogleSignUpResponse.from_dict(result.json()))
