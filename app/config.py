import os


class Config:
    SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI", "postgresql://user:password@postgres:5432/quotes-db")
    ASYNC_SQLALCHEMY_DATABASE_URI = os.getenv(
        "ASYNC_SQLALCHEMY_DATABASE_URI", "postgresql+asyncpg://user:password@postgres:5432/quotes-db"
    )
    LOGGING_LEVEL = "INFO"
    GOOGLE_API_KEY = os.getenv("GOOGLE_API_KEY", "")
