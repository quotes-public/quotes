from __future__ import annotations

from datetime import datetime

from sqlalchemy import Column, DateTime, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.decl_api import DeclarativeMeta

import app.data_types as dt

Base: DeclarativeMeta = declarative_base()


class Quote(Base):
    __tablename__ = "quote"
    id = Column(Integer, primary_key=True)
    quote = Column(String, nullable=False)
    category = Column(String, nullable=False)
    author = Column(String, nullable=True)
    timestamp = Column(DateTime, nullable=False, default=datetime.now)

    @classmethod
    def from_dataclass(cls, quote: dt.Quote) -> Quote:
        return cls(quote=quote.quote, category=quote.category, author=quote.author)

    def to_dataclass(self) -> dt.Quote:
        dataclass_instance = dt.Quote(quote=self.quote, category=self.category, author=self.author)
        # Convert timestamp to ISO 8601 format as a string
        dataclass_instance.timestamp = self.timestamp.isoformat() if self.timestamp else None

        return dataclass_instance
