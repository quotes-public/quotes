from dataclasses import dataclass, field
from datetime import datetime

from dataclasses_jsonschema import JsonSchemaMixin


class BaseType(JsonSchemaMixin):
    pass


@dataclass(slots=True)
class Quote(BaseType):
    quote: str | None = None
    category: str | None = None
    author: str | None = None
    timestamp: datetime | str | None = field(default=None, init=False)


@dataclass(slots=True)
class GetQuotesResponse(BaseType):
    quotes: list[Quote]


@dataclass(slots=True)
class User(BaseType):
    email: str
    password: str


@dataclass(slots=True)
class TokenValidity(BaseType):
    valid: bool
