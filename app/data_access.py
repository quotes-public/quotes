import firebase_admin
from fastapi import Header, HTTPException
from firebase_admin import auth, credentials


# Default Firebase credentials object for testing or fallback
class MockFirebaseCredentials:
    def __init__(self):
        # Set any necessary default attributes here
        pass


try:
    cred = credentials.Certificate("./app/.json") # firebase admin json
    firebase_admin.initialize_app(cred)
except FileNotFoundError:
    # Fallback to mock credentials in case the file is not found
    cred = MockFirebaseCredentials()


def evaluate_auth_header(auth_header, firebase_admin: firebase_admin) -> None:
    if auth_header:
        id_token = auth_header.split("Bearer ")[1]
    else:
        raise HTTPException(status_code=401, detail="Authorization header is missing")

    # Verify the ID token and extract the email
    try:
        decoded_token = auth.verify_id_token(id_token)
    except firebase_admin.auth.InvalidIdTokenError:
        raise HTTPException(status_code=401, detail="Invalid token")

    user_email = decoded_token.get("email")
    specific_email = "denisj123@hotmail.com"
    if user_email != specific_email:
        raise HTTPException(status_code=401, detail="Unauthorized: Token does not match specific email address")

    return None


def valid_token(id_token: str) -> bool:
    token = id_token.split("Bearer ")[1]
    try:
        decoded_token = auth.verify_id_token(token)
    except firebase_admin.auth.InvalidIdTokenError:
        raise HTTPException(status_code=401, detail="Invalid token")
    else:
        user_email = decoded_token.get("email")
        if user_email == "denisj123@hotmail.com":
            return True
        else:
            return False


async def token_auth_dependency(authorization: str = Header(None)) -> bool:
    if not authorization:
        raise HTTPException(status_code=401, detail="Authorization header is missing")
    try:
        evaluate_auth_header(authorization, firebase_admin)
    except HTTPException as http_exc:
        raise http_exc

    # If the token is valid, return something meaningful if needed
    return True  # or return decoded token or user information
