from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy.orm import sessionmaker

import app.data_types as dt
from app.models import Quote

# from structlog import get_logger


# logger = get_logger()


async def _get_quotes(engine, quote: dt.Quote | None = None) -> list[dt.Quote]:
    async with AsyncSession(engine) as session:
        # Build the query
        query = select(Quote)
        if quote:
            if quote.category:
                query = query.where(Quote.category == quote.category)
            if quote.author:
                query = query.where(Quote.author == quote.author)

        # Execute the query asynchronously
        result = await session.execute(query)
        quotes = result.scalars().all()

        # Convert to data types
        return [q.to_dataclass() for q in quotes]


async def submit_quote(async_engine, quote: dt.Quote) -> dt.Quote:
    quote_entry = Quote.from_dataclass(quote)
    async_session = sessionmaker(async_engine, expire_on_commit=False, class_=AsyncSession)
    async with async_session() as session:
        async with session.begin():
            session.add(quote_entry)
            await session.commit()

        # Refresh the instance to ensure all database-generated fields are up-to-date
        await session.refresh(quote_entry)

    return quote_entry.to_dataclass()
