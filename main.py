import databases
import sqlalchemy
from fastapi import Depends, FastAPI, HTTPException, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from sqlalchemy.ext.asyncio import create_async_engine

import app.data_types as dt
from app.config import Config
from app.data_access import token_auth_dependency, valid_token
from app.data_queries import _get_quotes, submit_quote
from app.google_data_queries import sign_in_user, sign_up_user

metadata = sqlalchemy.MetaData()

database = databases.Database(Config.SQLALCHEMY_DATABASE_URI)

engine = sqlalchemy.create_engine(Config.SQLALCHEMY_DATABASE_URI)
async_engine = create_async_engine(Config.ASYNC_SQLALCHEMY_DATABASE_URI)

metadata.create_all(engine)

app = FastAPI()

# Allow all origins for testing. In production, specify your allowed origins.
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Set this to the list of allowed origins
    allow_credentials=True,
    allow_methods=["*"],  # Set this to the list of allowed HTTP methods
    allow_headers=["*"],  # Set this to the list of allowed headers
)


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/sign-up")
async def sign_up(user: dt.User):
    sign_up = sign_up_user(user.email, user.password)
    return sign_up


@app.post("/sign-in")
async def sign_in(user: dt.User):
    sign_in = sign_in_user(user.email, user.password)
    return sign_in


@app.get("/validate-token")
async def validate_token(request: Request) -> JSONResponse:
    token = request.headers.get("authorization")
    if token is None:
        raise HTTPException(status_code=403, detail="No token provided")

    try:
        is_valid = valid_token(token)
    except HTTPException as http_exc:
        raise http_exc

    if not is_valid:
        raise HTTPException(
            status_code=403,
            detail="The client does not have permission to access \
            the requested resource due to invalid or expired authentication credentials",
        )

    return JSONResponse(content=dt.TokenValidity(is_valid).to_dict(), status_code=200)


@app.get("/get-quotes")
async def get_quotes(request: Request, submitted_quote: dt.Quote | None = None) -> JSONResponse:
    quotes = await _get_quotes(async_engine, submitted_quote)
    return JSONResponse(content=dt.GetQuotesResponse(quotes).to_dict(), status_code=200)


@app.put("/quotes")
async def add_quote(submitted_quote: dt.Quote, token_valid: bool = Depends(token_auth_dependency)) -> JSONResponse:
    # If the code reaches this point, it means the token is valid
    new_quote = await submit_quote(async_engine, submitted_quote)
    return JSONResponse(content=new_quote.to_dict(), status_code=201)
