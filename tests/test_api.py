import pytest
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import create_async_engine

from main import app

engine = create_async_engine("postgresql+asyncpg://user:password@postgres:5432/quotes-db")


@pytest.mark.asyncio
async def test_read_main(test_db):
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get("/get-quotes?category=Bible")
    assert response.status_code == 200
    print(response.json())
    assert response.json()["quotes"][0]["quote"] == "Quote 1"
    assert response.json()["quotes"][0]["category"] == "Bible"
    assert response.json()["quotes"][1]["quote"] == "Quote 2"
    assert response.json()["quotes"][1]["category"] == "Bible"
