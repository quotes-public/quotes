import pytest_asyncio
from fastapi.testclient import TestClient
from firebase_admin import credentials
from sqlalchemy.ext.asyncio import create_async_engine

import app.data_types as dt
from app.data_queries import submit_quote
from app.models import Base  # Import the Base metadata from your models
from main import app

client = TestClient(app)

# test setup file or at the start of your test scripts

engine = create_async_engine("postgresql+asyncpg://user:password@postgres:5432/quotes-db")


class MockCertificateObject:
    pass


def mock_credentials_certificate(path):
    # You can return a mock object or a suitable replacement here
    return MockCertificateObject()


def test_some_function(monkeypatch):
    monkeypatch.setattr(credentials, "Certificate", mock_credentials_certificate)

    # Now, when you call a function from main.py that triggers credentials.Certificate,
    # it will use the mock_credentials_certificate function instead.


async def setup_database():
    # Create tables based on the models you have defined
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

    return engine


async def teardown_database():
    # Drop all tables
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)


@pytest_asyncio.fixture
async def test_db():
    # Set up the database
    engine_2 = await setup_database()
    print("engine setup")

    # ... Insert test data if needed ...
    await submit_quote(engine_2, dt.Quote(quote="Quote 1", category="Bible", author="Mark"))
    await submit_quote(engine_2, dt.Quote(quote="Quote 2", category="Bible", author="Mark"))
    await submit_quote(engine_2, dt.Quote(quote="Quote 3", category="Philosophy", author="Camus"))
    yield engine_2  # This allows the test to run with the database in this state

    # Tear down the database
    await teardown_database()
