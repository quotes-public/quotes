import { defineStore } from 'pinia'
export const useAuthStore = defineStore('auth', {
  state: () => ({
    idToken: null as string | null, // Initialize idToken as null
  }),
  actions: {
    setIdToken(token: string) {
      this.idToken = token
    },
  },
  persist: {
    enabled: true,
    strategies: [
      {
        key: 'auth',
        storage: localStorage, // or sessionStorage
      },
    ],
  },
})
