// webpack.config.js
module.exports = {
    // ... other parts of your webpack configuration
    resolve: {
      fallback: {
        http: require.resolve('stream-http'),
        https: require.resolve('https-browserify'),
        stream: require.resolve('stream-browserify'),
        zlib: require.resolve('browserify-zlib')
      },
    },
  };
  