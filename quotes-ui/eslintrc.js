module.exports = {
    extends: [
      // other configs e.g. 'plugin:vue/essential'
      'prettier',
      'prettier/vue'
    ],
    plugins: [
      // other plugins e.g. 'vue'
      'prettier'
    ],
    "ignorePatterns": ["dist/*"],
    rules: {
      // override/add rules settings here, such as:
      'prettier/prettier': ['error', { 'endOfLine': 'auto' }],
    }
  };
  