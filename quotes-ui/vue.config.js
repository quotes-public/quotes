// vue.config.js
module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: 'http://web:8080/', // Proxy API requests to FastAPI server
        changeOrigin: true
      }
    }
  },

  pluginOptions: {
    vuetify: {
			// https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vuetify-loader
		}
  }
};
