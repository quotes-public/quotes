# Use an official Python runtime as a parent image
FROM python:3.11-slim-buster

# Set the working directory in the container
WORKDIR /usr/src

# Install pipenv
# RUN pip install uvicorn[standard]
RUN pip install alembic

# Copy the dependencies file to the working directory
COPY start.sh Pipfile Pipfile.lock ./

# Install dependencies (this will create the Pipfile.lock)
RUN pip install pipenv
RUN pipenv install --deploy

ENV PYTHONPATH="."

# Copy the content of the local src directory to the working directory
COPY . .

# Command to run on container start
CMD ["./start.sh"]


